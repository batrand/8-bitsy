﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RisingWater : MonoBehaviour
{
    public float Speed = 0;

    private void Update()
    {
        if (Speed == 0) return;
        var newPosition = transform.position;
        newPosition.y += Speed * Time.deltaTime;
        transform.position = newPosition;
    }

    public void SetSpeed(float speed)
    {
        Speed = speed;
    }
}
