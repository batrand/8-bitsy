﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// DIRTY SHAMEFUL HACK IM SO SORRY IM ASHAMED OF MYSELF
[RequireComponent(typeof(PointsPatrol))]
public class MantisAnimator : MonoBehaviour
{
    private PointsPatrol _patrol;
    private float _leftX, _rightX;

    private void Start()
    {
        _patrol = GetComponent<PointsPatrol>();
        _rightX = transform.localScale.x;
        _leftX = -_rightX;
    }

    private void Update()
    {
        var currentScale = transform.localScale;
        if (!_patrol.IsReversing) currentScale.x = _rightX;
        else currentScale.x = _leftX;
        transform.localScale = currentScale;
    }
}
