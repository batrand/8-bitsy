﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DelayedTrigger : MonoBehaviour
{
    public float Delay = 3f;
    public UnityEvent OnDelayElapsed;
    public void Trigger()
    {
        Invoke(nameof(TriggerElapsedEvent), Delay);
    }
    public void Trigger(float delay)
    {
        Invoke(nameof(TriggerElapsedEvent), delay);
    }
    public void TriggerElapsedEvent()
    {
        OnDelayElapsed.Invoke();
    }
}
