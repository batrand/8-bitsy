﻿using UnityEngine;

public static class Rigidbody2DExtensions
{
    public static void RemoveAllVelocity(this Rigidbody2D rb)
    {
        rb.angularVelocity = 0;
        rb.velocity = Vector2.zero;
    }
}