﻿using System.Collections.Generic;
using UnityEngine;

public static class TransformExtensions
{
    /// <summary>
    /// Raycasts from this Transform to a Target Position, and return a RaycastHit2D
    /// that indicates an unblocked hit.
    /// The resulting hit, if found, will not be the Transform itself.
    /// </summary>
    /// <param name="transform">The host object</param>
    /// <param name="target">The end position of this raycast</param>
    /// <param name="infinite">If true, raycast will not have a limit</param>
    /// <param name="ignoreTag">Objects with this tag will not be considered a block</param>
    /// <param name="maxLength">The maximum length of the raycast</param>
    /// <returns>A Raycast2DHit that is guaranteed to not be this object (a Unity quirk)</returns>
    public static RaycastHit2D? FindUnblockedHit(this Transform transform, Vector3 target, 
        bool infinite = false, string ignoreTag = "", float maxLength = -1f)
    {
        return transform.FindUnblockedHit(Vector3.zero, target, infinite, ignoreTag, maxLength);
    }

    /// <summary>
    /// Raycasts from this Transform to a Target Position, and return a RaycastHit2D
    /// that indicates an unblocked hit.
    /// The resulting hit, if found, will not be the Transform itself.
    /// </summary>
    /// <param name="transform">The host object</param>
    /// <param name="origin">The originating point.</param>
    /// <param name="target">The end position of this raycast</param>
    /// <param name="infinite">If true, raycast will not have a limit</param>
    /// <param name="ignoreTag">Objects with this tag will not be considered a block</param>
    /// <param name="maxLength">The maximum length of the raycast</param>
    /// <returns>A Raycast2DHit that is guaranteed to not be this object (a Unity quirk)</returns>
    public static RaycastHit2D? FindUnblockedHit(this Transform transform, Vector3 origin, Vector3 target, 
        bool infinite = false, string ignoreTag = "", float maxLength = -1f)
    {
        var position = transform.position;
        if (origin != Vector3.zero) position = origin;

        // Get vector pointing from this transform's position to target position
        var direction = target - position;
        var direction2d = new Vector2(direction.x, direction.y);

        // Calculate distance between this transform and target
        var position2d = new Vector2(position.x, position.y);
        var targetPosition2d = new Vector2(target.x, target.y);
        var distance = Vector2.Distance(position2d, targetPosition2d);

        // Get all hit objects
        RaycastHit2D[] hits;
        if (!infinite)
        {
            if (maxLength > 0) hits = Physics2D.RaycastAll(position, direction2d, maxLength);
            else hits = Physics2D.RaycastAll(position, direction2d, distance);
        }
        else hits = Physics2D.RaycastAll(position, direction2d);

        // Unity raycasting also detects the collider of the originating
        // gameobject.
        // The validation here is to ensure there are no obstacles blocking
        // this object and the target webbable.

        // If there is only one hit found, and in some future where Unity
        // has fixed this quirk, the first hit will be valid when it is
        // not this object.
        if (hits.Length == 1)
        {
            var hit = hits[0];
            if (transform.gameObject.GetInstanceID() != hit.transform.gameObject.GetInstanceID())
                return hit;
        }
        
        for (int i = 1; i < hits.Length; i++)
        {
            var hit = hits[i];
            var hitObj = hit.transform.gameObject;
            var thisGObj = transform.gameObject;
            var thisGObjsId = thisGObj.GetInstanceID();

            // Only process this when ignore tag is specified
            if (ignoreTag != "")
            {
                // If this object should be ignored, skip it.
                if (hitObj.HasTag(ignoreTag)) continue;

                // The hit will be valid when all hits between this gameobject
                // and the gameobject of the hit have ignore tag

                // Get all previous hits
                var previousHits = new List<RaycastHit2D>(hits)
                    .GetRange(0, i)
                    .ToArray();

                // Find the index of this object
                int indexOfThisObject = -1;
                for (int x = 0; x < previousHits.Length; x++)
                {
                    var previousHitsId = previousHits[x].transform.gameObject.GetInstanceID();
                    if (previousHitsId == thisGObjsId) indexOfThisObject = x;
                }

                // Cannot find this object in previous hit? Should not happen!
                if (indexOfThisObject == -1)
                {
                    Debug.LogError("Cannot find this object in previous hits!");
                    return null;
                }

                // If there is any object that does not have ignore tag, 
                // this hit is invalid
                for (int x = indexOfThisObject; x < previousHits.Length; x++)
                {
                    if (!previousHits[x].transform.gameObject.HasTag(ignoreTag))
                        continue;
                }

                // Otherwise, it is valid.
                return hit;

            }

            // No ignore tag specified
            else
            {
                var previousHit = hits[i - 1];
                var previousHitGObj = previousHit.transform.gameObject;
                // For there to be no obstacles between originator and target,
                // the previous hit should be this gameobject
                if (previousHitGObj.GetInstanceID() == thisGObj.GetInstanceID())
                    return hit;
            }
        }

        // None found
        return null;
    }
}