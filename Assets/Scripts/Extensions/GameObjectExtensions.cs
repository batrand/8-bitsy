﻿using System;
using System.Linq;
using UnityEngine;

public static class GameObjectExtensions
{
    public static bool HasTag(this GameObject gobj, string tag)
    {
        var tags = gobj.GetComponent<Tags>();
        if (tags != null && tags.TagsArray.Contains(tag)) return true;
        else return false;
    }

    public static bool HasComponent<T>(this GameObject gobj)
    {
        if (gobj.GetComponent<T>() != null) return true;
        else return false;
    }

    public static bool HasComponent(this GameObject gobj, Type type)
    {
        if (gobj.GetComponent(type) != null) return true;
        else return false;
    }
}