﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BitsySounds : MonoBehaviour
{
    [Range(0,1)]
    public float Volume = 0.1f;
    public AudioClip WalkClip;
    public AudioClip JumpClip;
    public AudioClip WebClip;
    public AudioClip WebFailClip;
    public AudioClip DieClip;

    private AudioSource _walkSource;
    private AudioSource _jumpSource;
    private AudioSource _webSource;
    private AudioSource _dieSource;

    private void Start()
    {
        _walkSource = gameObject.AddComponent<AudioSource>();
        _jumpSource = gameObject.AddComponent<AudioSource>();
        _webSource = gameObject.AddComponent<AudioSource>();
        _dieSource = gameObject.AddComponent<AudioSource>();
    }

    private void Update()
    {
        _walkSource.volume = Volume;
        _jumpSource.volume = Volume;
        _webSource.volume = Volume;
    }

    public void Walk()
    {
        if(_walkSource.clip != WalkClip)
            _walkSource.clip = WalkClip;
        if(!_walkSource.isPlaying)
            _walkSource.Play();
    }

    public void StopWalk()
    {
        _walkSource.Stop();
    }

    public void Jump() => StopAndPlay(_jumpSource, JumpClip);
    public void Web() => StopAndPlay(_webSource, WebClip);
    public void FailedWeb() => StopAndPlay(_webSource, WebFailClip);
    public void Die() => StopAndPlay(_dieSource, DieClip);

    private void StopAndPlay(AudioSource src, AudioClip clip)
    {
        src.Stop();
        src.clip = clip;
        src.Play();
    }
}
