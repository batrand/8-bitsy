﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointsPatrol : MonoBehaviour
{
    public Transform[] Points;
    public float Speed;
    public bool IsReversing;

    private int _currentPointIndex = 0;
    private void Update()
    {
        if (_currentPointIndex < Points.Length)
            UpdatePosition();
        else OnGotToLastPoint();

        // Hack to indicate flipped direction.
        if (_currentPointIndex == 0) IsReversing = true;
        else IsReversing = false;
    }

    private void UpdatePosition()
    {
        var target = Points[_currentPointIndex];
        transform.position = Vector3.MoveTowards(
            transform.position, target.position,
            Speed * Time.deltaTime);

        if (Vector3.Distance(transform.position, target.position) < .1f)
            _currentPointIndex++;
    }

    private void OnGotToLastPoint()
    {
        _currentPointIndex = 0;
    }

    public void SetSpeed(float speed)
    {
        Speed = speed;
    }
}
