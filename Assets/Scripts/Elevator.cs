﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Elevator : MonoBehaviour
{
    public float MoveSpeed = 1f;
    public Vector2[] Targets;
    public bool IsMoving { get; private set; }

    public void GoTo(int targetIndex)
    {
        StartCoroutine(nameof(GoToCoroutine), targetIndex);
    }

    private int _queuedIndex = -1;
    private IEnumerator GoToCoroutine(int targetIndex)
    {
        // Queue if already moving
        if (IsMoving)
        {
            _queuedIndex = targetIndex;
            yield break;
        }

        IsMoving = true;
        Vector3 targetPos3 = Targets[targetIndex];
        while (transform.position != targetPos3)
        {
            transform.position = Vector3.MoveTowards(transform.position, targetPos3, MoveSpeed * Time.deltaTime);
            yield return null;
        }

        IsMoving = false;
        yield return null;

        if (_queuedIndex != -1)
        {
            GoTo(_queuedIndex);
            _queuedIndex = -1;
        }
    }
}
