﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class BackgroundMusic : MonoBehaviour
{
    public static BackgroundMusic Instance;

    public AudioClip ThemeClip;
    public AudioClip BackgroundClip;
    public AudioClip TenseClip;
    public AudioClip ChaseClip;
    public AudioClip ReunionClip;

    private AudioSource _src;

    private void Start()
    {
        if (Instance == null) Instance = this;
        else return;

        _src = GetComponent<AudioSource>();
        MainMenu();

        DontDestroyOnLoad(gameObject);
    }

    private void PlayClip(AudioClip clip)
    {
        if (_src.clip == clip) return;
        _src.clip = clip;
        _src.Play();
    }

    public void Stop()
    {
        _src.Stop();
    }

    public void Resume()
    {
        _src.Play();
    }

    public void MainMenu()
    {
        PlayClip(ThemeClip);
    }

    public void NormalLevels()
    {
        PlayClip(BackgroundClip);
    }

    public void Tense()
    {
        PlayClip(TenseClip);
    }

    public void Chase()
    {
        PlayClip(ChaseClip);
    }

    public void Reunion()
    {
        if (_src.clip == ReunionClip) return;

        _src.Stop();
        _src.clip = ReunionClip;
        _src.PlayOneShot(ReunionClip);
    }
}
