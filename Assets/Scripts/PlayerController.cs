﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.Experimental.UIElements;
using Edge = PlayerEdgeStates.Edge;
using EdgeState = PlayerEdgeStates.EdgeState;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Collider2D))]
[RequireComponent(typeof(PlayerEdgeStates))]
[RequireComponent(typeof(Webber))]
[RequireComponent(typeof(BitsySounds))]
public class PlayerController : MonoBehaviour
{
    [Header("Movement")]
    public string MoveLeftButton = "a";
    public string MoveRightButton = "d";
    public float MoveSpeed = 5f;
    public bool IsMoving = false;

    [Space]

    [Header("Jumping")]
    public string JumpButton = "space";
    public float JumpForce = 2000f;
    public float DownForce = 1500f;
    public Vector2 TerminalVelocity = new Vector2(10,10);

    [Space]

    [Header("Sticking")]
    public string UnstickButton = "s";
    [Tooltip("Number of seconds to unstick before getting stuck again")]
    public float UnstickGrace = 0.5f;

    private Rigidbody2D _rb;
    private Collider2D _collider;
    private float _rbGravityScale;
    private void EnableGravity(bool enabled)
    {
        if (enabled) _rb.gravityScale = _rbGravityScale;
        else _rb.gravityScale = 0;
    }
    private void ApplyGravMultiplier(float multiplier)
    {
        if (multiplier == 0)
            _rb.gravityScale = _rbGravityScale;
        else
        {
            var multipliedScale = _rbGravityScale * multiplier;
            _rb.gravityScale *= multipliedScale;
        }
    }

    private PlayerEdgeStates _states;
    private Webber _webber;
    private BitsySounds _sounds;

    private void Start()
    {
        _rb = GetComponent<Rigidbody2D>();
        _rbGravityScale = _rb.gravityScale;
        _states = GetComponent<PlayerEdgeStates>();
        _webber = GetComponent<Webber>();
        _sounds = GetComponent<BitsySounds>();
        _collider = GetComponent<Collider2D>();
    }

    private float _currentSpeed;
    private void Update()
    {
        IsMoving = _currentSpeed != 0;
        _rb.velocity = new Vector2(_currentSpeed, _rb.velocity.y);

        if (IsMoving && IsTouching(Edge.Bottom)) _sounds.Walk();
        else _sounds.StopWalk();

        // Left and Right
        if (Input.GetKey(MoveLeftButton) && !IsStuck() && !IsWebbing)
            SetCurrentSpeedToLeft();
        if (Input.GetKeyUp(MoveLeftButton))
            OnStopMoving();
        if (Input.GetKey(MoveRightButton) && !IsStuck() && !IsWebbing)
            SetCurrentSpeedToRight();
        if (Input.GetKeyUp(MoveRightButton))
            OnStopMoving();

        // Jumping
        if (Input.GetKeyDown(JumpButton) && CanJump() && !IsStuck() && !IsWebbing)
        {
            AddJumpUpForce();
            _sounds.Jump();
        }

        if (Input.GetKeyUp(JumpButton) && IsInAir()
            // When already fally down, velocity.y will be negative
            // If it's already fallint down, no need for down force                          
            && _rb.velocity.y > 0)
            AddDownForce();

        // Auto-jumping
        if (CanJump() && ShouldAutoJump)
            AddJumpUpForce();

        // Stickiness
        if (IsStuck()) Stick(); else EnableGravity(true);
        if (Input.GetKeyDown(UnstickButton)) UnStick(true);

        // Sticking to right edge
        if (_states.IsStuck(Edge.Right))
        {
            // Jump button: jumps straight up
            if (Input.GetKeyDown(JumpButton))
            {
                UnStick(true);
                _sounds.Jump();
                AddJumpUpForce();
            }

            // Hold left and jump: jump to the left away from the webbable
            if (Input.GetKey(MoveLeftButton) && Input.GetKeyDown(JumpButton))
            {
                UnStick(true);
                _sounds.Jump();
                SetCurrentSpeedToLeft();
            }
        }

        // Sticking to left edge
        if (_states.IsStuck(Edge.Left))
        {
            // Jump button: jumps straight up
            if (Input.GetKeyDown(JumpButton))
            {
                UnStick(true);
                _sounds.Jump();
                AddJumpUpForce();
            }

            // Hold right and jump: jump to the right away from the webbable
            if (Input.GetKey(MoveRightButton) && Input.GetKeyDown(JumpButton))
            {
                UnStick(true);
                _sounds.Jump();
                SetCurrentSpeedToRight();
            }
        }

        // Sticking to top edge
        if (_states.IsStuck(Edge.Top))
        {
            // Hold left and jump: jump to the left
            if (Input.GetKey(MoveLeftButton) && Input.GetKeyDown(JumpButton))
            {
                UnStick(true);
                SetCurrentSpeedToLeft();
                _sounds.Jump();
                _rb.AddForce(new Vector2(-JumpForce, _rb.velocity.y));
            }

            // Hold right and jump: jump to the right
            if (Input.GetKey(MoveRightButton) && Input.GetKeyDown(JumpButton))
            {
                UnStick(true);
                SetCurrentSpeedToRight();
                _sounds.Jump();
                _rb.AddForce(new Vector2(JumpForce, _rb.velocity.y));
            }
        }

        // Slippery slope
        if (_states.IsSlipperySlope(Edge.Bottom) && !Input.GetKey(JumpButton))
            ApplyGravMultiplier(_states.GetSlopeMultiplier(Edge.Bottom));

        // Clamping to terminal velocity
        if (_rb.velocity.y > TerminalVelocity.y)
            _rb.velocity = new Vector2(_rb.velocity.x, TerminalVelocity.y);
        if (_rb.velocity.x > TerminalVelocity.x)
            _rb.velocity = new Vector2(TerminalVelocity.x, _rb.velocity.y);
        if (_rb.velocity.y < -TerminalVelocity.y)
            _rb.velocity = new Vector2(_rb.velocity.x, -TerminalVelocity.y);
        if (_rb.velocity.x < -TerminalVelocity.x)
            _rb.velocity = new Vector2(-TerminalVelocity.x, _rb.velocity.y);
    }

    private void FixedUpdate()
    {
        if (_hasPendingJumpUpForce)
        {
            _rb.AddForce(new Vector2(_rb.velocity.x, JumpForce));
            _hasPendingJumpUpForce = false;
        }

        if (_hasPendingDownForce)
        {
            _rb.AddForce(new Vector2(_rb.velocity.x, -DownForce));
            _hasPendingDownForce = false;
        }
    }

    private bool _hasPendingJumpUpForce = false;
    private void AddJumpUpForce() => _hasPendingJumpUpForce = true;
    private bool _hasPendingDownForce = false;
    private void AddDownForce() => _hasPendingDownForce = true;

    public void UnStick(bool statesToo)
    {
        if(statesToo) _states.Unstick(UnstickGrace);
        EnableGravity(true);
    }
    public void Stick()
    {
        EnableGravity(false);
        _currentSpeed = 0;
        _rb.RemoveAllVelocity();
    }
    public void SetCurrentSpeedToLeft()
    {
        _currentSpeed = -MoveSpeed;
        if (IsTouching(Edge.Left)) _currentSpeed = 0;
    }
    public void SetCurrentSpeedToRight()
    {
        _currentSpeed = MoveSpeed;
        if (IsTouching(Edge.Right)) _currentSpeed = 0;
    }
    public void SetCurrentSpeedToZero()
    {
        _currentSpeed = 0;
    }
    private void OnStopMoving()
    {
        if (_states.IsSlippery(Edge.Bottom))
            Invoke(nameof(SetCurrentSpeedToZero), _states.GetSlipSeconds(Edge.Bottom));
        else SetCurrentSpeedToZero();
    }

    public void Die()
    {
        AddJumpUpForce();
        _sounds.Die();
        _collider.isTrigger = true;
        _states.enabled = false;
    }

    public bool IsStuck() => _states.IsStuck(Edge.Bottom)
                              || _states.IsStuck(Edge.Top)
                              || _states.IsStuck(Edge.Left)
                              || _states.IsStuck(Edge.Right);

    private bool IsWebbing => _webber.IsWebbing;

    private bool CanJump() => _states.CanJump(Edge.Bottom);

    private bool IsInAir() => _states.IsInAir(Edge.Top)
                              && _states.IsInAir(Edge.Bottom)
                              && _states.IsInAir(Edge.Left)
                              && _states.IsInAir(Edge.Right);

    private bool IsTouching(Edge edge) => _states.EdgeStates[edge] == EdgeState.Touching;

    private bool ShouldAutoJump => _states.ShouldAutoJump(Edge.Bottom)
                                   || _states.ShouldAutoJump(Edge.Left)
                                   || _states.ShouldAutoJump(Edge.Right);
}
