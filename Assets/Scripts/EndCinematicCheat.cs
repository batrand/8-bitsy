﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndCinematicCheat : MonoBehaviour
{
    public LevelLoader Loader;
    private void Update()
    {
        if(Input.GetKeyDown("e"))
            Loader.Load("level4_widescreen_end");
    }
}
