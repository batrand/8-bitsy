﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CameraController : MonoBehaviour
{
    public Transform player;

    public float LowestCameraY = 0;
    public float LowestFollowY = -1.8f;
    public float HighestCameraY;
    private float _highestFollowY => (HighestCameraY - LowestCameraY) + LowestFollowY;

	void Update ()
	{
	    if (player == null) return;

	    if (transform.position.y >= LowestCameraY 
	        && player.position.y > LowestFollowY 
	        && transform.position.y <= HighestCameraY 
	        && player.position.y < _highestFollowY)
	    {
	        transform.position = new Vector3(transform.position.x, 
                player.position.y + Mathf.Abs(LowestFollowY), 
	            transform.position.z);
	    }
    }

    public void SetPlayer(Transform player)
    {
        this.player = player;
    } 
}
