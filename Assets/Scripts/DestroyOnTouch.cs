﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class DestroyOnTouch : MonoBehaviour
{
    public string KillerTag = "KILLER";
    public float Delay = 0.2f;
    public bool DestroyParent = true;

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.HasTag(KillerTag))
        {
            Destroy(gameObject, Delay);
            if (DestroyParent) Destroy(transform.parent.gameObject, Delay);
        }
    }
}
