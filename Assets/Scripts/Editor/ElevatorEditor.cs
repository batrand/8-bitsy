﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Elevator))]
public class ElevatorEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        var elevator = (Elevator) target;

        if (GUILayout.Button("Start Move"))
        {
            elevator.GoTo(0);
        }
    }
}
