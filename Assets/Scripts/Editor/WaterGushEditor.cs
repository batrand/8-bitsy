﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(WaterGush))]
public class WaterGushEditor: Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        var waterGush = (WaterGush)target;

        if (GUILayout.Button("Open"))
        {
            waterGush.StartOpening();
        }
        if (GUILayout.Button("Close"))
        {
            waterGush.StartClosing();
        }
    }
}