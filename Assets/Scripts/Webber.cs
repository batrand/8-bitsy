﻿using System;
using System.Collections;
using UnityEngine;
using Edge = PlayerEdgeStates.Edge;
using EdgeState = PlayerEdgeStates.EdgeState;

[RequireComponent(typeof(LineRenderer))]
[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(PlayerController))]
[RequireComponent(typeof(PlayerEdgeStates))]
[RequireComponent(typeof(BitsySounds))]
public class Webber : MonoBehaviour
{
    public float MoveSpeed = 5f;
    public float WebRange = -1f;

    private LineRenderer _webRenderer;
    private Rigidbody2D _rb;
    private PlayerController _controller;
    private PlayerEdgeStates _states;
    private BitsySounds _sounds;

    [Tooltip("Add this tag to a Tags component and that object will not interrupt Webber's movement")]
    public string IgnoreTag = "IGNORE";

    public bool IsWebbing = false;
    public bool IsMovingToTarget = false;
    public bool ShouldFreeze = false;

    private void Start()
    {
        _webRenderer = GetComponent<LineRenderer>();
        _rb = GetComponent<Rigidbody2D>();
        _controller = GetComponent<PlayerController>();
        _states = GetComponent<PlayerEdgeStates>();
        _sounds = GetComponent<BitsySounds>();
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0) && !IsWebbing)
        {
            var hit = transform.FindUnblockedHit(
                target: Camera.main.ScreenToWorldPoint(Input.mousePosition),
                infinite: false,
                ignoreTag: IgnoreTag,
                maxLength: WebRange);
            if (hit == null)
            {
                _sounds.FailedWeb();
                return;
            }

            _sounds.Web();
            var targetWebbable = hit.Value.transform.gameObject.GetComponent<Webbable>();

            // If hit has no webbable, or webbable is occupied,
            // web to it then animate back.
            if (targetWebbable == null || targetWebbable.IsOccupied)
            {
                // Do not web to the webbable if you are occupying it
                if (_states.HasTouchable(targetWebbable)) return;

                StartCoroutine(nameof(AnimateWebToTarget), new AnimateWebToTargetArgs()
                {
                    targetPosition = hit.Value.point,
                    animateBack = true
                });
                return;
            }

            // If can web to it, web to it then move to it.
            StartCoroutine(nameof(AnimateWebToTarget), new AnimateWebToTargetArgs()
            {
                targetPosition = hit.Value.point,
                moveToTarget = true
            });
        }

        if (ShouldFreeze) _rb.constraints = RigidbodyConstraints2D.FreezeAll;
        else _rb.constraints = RigidbodyConstraints2D.FreezeRotation;
    }

    private struct AnimateWebToTargetArgs
    {
        public Vector2 targetPosition;
        public bool animateBack;
        public bool moveToTarget;
    }

    /// <summary>
    /// Animate the web string from Webber to the target
    /// </summary>
    private IEnumerator AnimateWebToTarget(AnimateWebToTargetArgs args)
    {
        _controller.Stick();
        IsWebbing = true;
        ShouldFreeze = true;

        _webRenderer.SetPositions(new[] { transform.position, transform.position });

        while (Vector3.Distance(WebEndPosition, args.targetPosition) > 0)
        {
            var newEndPosition = Vector2.MoveTowards(WebEndPosition, args.targetPosition, MoveSpeed * Time.deltaTime);
            _webRenderer.SetPosition(1, newEndPosition);
            yield return 0; // wait for next frame
        }

        if (args.animateBack) StartCoroutine(nameof(AnimateWebBack));
        if (args.moveToTarget) StartCoroutine(nameof(MoveAlongWebToTarget), args.targetPosition);
    }

    /// <summary>
    /// Animate the web string back to Webber.
    /// </summary>
    private IEnumerator AnimateWebBack()
    {
        ShouldFreeze = true;
        var targetPosition = transform.position;
        while (Vector3.Distance(WebEndPosition, targetPosition) > 0)
        {
            var newEndPosition = Vector2.MoveTowards(WebEndPosition, targetPosition, MoveSpeed * Time.deltaTime);
            _webRenderer.SetPosition(1, newEndPosition);
            if (IsMoving) break;
            yield return 0; // wait for next frame
        }
        DeleteLine(); // delete line
        _controller.UnStick(false);
        IsWebbing = false;
        ShouldFreeze = false;
    }

    private IEnumerator MoveAlongWebToTarget(Vector2 targetPosition)
    {
        ShouldFreeze = false;
        IsMovingToTarget = true;
        Vector3 targetPosition3 = targetPosition;
        _webRenderer.SetPositions(new[] { transform.position, targetPosition3 });
        
        _controller.Stick();
        _shouldStopMoveIn = false;
        while (Vector3.Distance(transform.position, targetPosition) > 0 
               && !_shouldStopMoveIn) // Once touched a collider, stop any further moving in attempt.
        {
            // Update the position and the web string
            var newPosition = Vector3.MoveTowards(transform.position, targetPosition, MoveSpeed*Time.deltaTime);
            _rb.MovePosition(newPosition);
            _webRenderer.SetPosition(0, transform.position);
            yield return new WaitForFixedUpdate();
        }
        _shouldStopMoveIn = false;

        // Cleanup
        DeleteLine();
        _rb.RemoveAllVelocity();
        IsWebbing = false;
        IsMovingToTarget = false;
    }

    private bool _shouldStopMoveIn = false;
    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.HasTag(IgnoreTag)) return;
        _shouldStopMoveIn = true;
    }

    private void DeleteLine() { _webRenderer.SetPositions(new[] { Vector3.zero, Vector3.zero }); }
    private Vector3 WebEndPosition => _webRenderer.GetPosition(1);
    private bool IsMoving => _controller.IsMoving;
}
