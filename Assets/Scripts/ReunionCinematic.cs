﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ReunionCinematic : MonoBehaviour
{
    public TextMeshPro SpeechBubble;
    public BitsyAnimator BitsyAnimator;
    public SpriteRenderer BitsyRenderer;
    public PointsPatrol Elevator;
    public Animator GirlSpiderAnimator;
    public RuntimeAnimatorController EatingAnimation;
    public LevelLoader LevelLoader;

    public void StartCinematic()
    {
        StartCoroutine(nameof(CinematicCoroutine));
    }

    private IEnumerator CinematicCoroutine()
    {
        Elevator.SetSpeed(0);

        yield return new WaitForSeconds(1.8f);
        SpeechBubble.gameObject.SetActive(true);
        SpeechBubble.text = "Oh Bitsy! You managed to" +
                            "\nfind me at the top of" +
                            "\nthe water spout!";
        yield return new WaitForSeconds(7.9f);
        SpeechBubble.text = "As a token of our love...";
        yield return new WaitForSeconds(4f);
        SpeechBubble.text = "here's your reward.";
        BitsyAnimator.enabled = true;
        yield return new WaitForSeconds(5f);

        SpeechBubble.text = "";
        SpeechBubble.gameObject.SetActive(false);
        GirlSpiderAnimator.runtimeAnimatorController = EatingAnimation;
        yield return new WaitForSeconds(1f);
        BitsyRenderer.enabled = false;
        yield return new WaitForSeconds(1.5f);

        LevelLoader.Load("story_end");
    }
}
