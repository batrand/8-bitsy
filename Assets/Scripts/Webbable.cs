﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class Webbable : MonoBehaviour
{
    public bool IsSticky = false;

    public bool IsOccupied = false;

    private void OnCollisionEnter2D(Collision2D other)
    {
        var webber = other.gameObject.GetComponent<Webber>();

        // Only get occupied by a webber that is webbing towards this
        if (webber != null && webber.IsWebbing) IsOccupied = true;
    }

    private void OnCollisionExit2D(Collision2D other)
    {
        if (other.gameObject.HasComponent<Webber>()) IsOccupied = false;
    }

    public bool ShouldStick => IsSticky && IsOccupied;
}
