﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelLoader : MonoBehaviour
{
    public string LevelName;
    public bool LoadOnCollision;

    public void Load() => SceneManager.LoadScene(LevelName);
    public void Load(string levelName) => SceneManager.LoadScene(levelName);

    private void OnCollisionEnter2D()
	{
	    if (LoadOnCollision) Load();
	}
}
