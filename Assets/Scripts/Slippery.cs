﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class Slippery : MonoBehaviour
{
    [Tooltip("When you let go, you will slip for this many seconds")]
    public float SlipSeconds = 0.5f;

    public bool IsSlope = false;
    public float SlopeGravityMultiplier = 20f;
}
