﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BitsyDeath : MonoBehaviour
{
    public Sprite DeathSprite;

    public void Die()
    {
        var controller = GetComponent<PlayerController>();
        if (controller != null) controller.Die();

        var webber = GetComponent<Webber>();
        if (webber != null) webber.enabled = false;

        // Disable animator and replace sprite renderer
        var animator = GetComponent<Animator>();
        if (animator != null) animator.enabled = false;
        var spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
        spriteRenderer.sprite = DeathSprite;

        // Stop camera from following Bitsy
        var camera = GameObject.FindObjectOfType<CameraController>();
        if (camera != null) camera.player = null;

        Destroy(gameObject, 2f);
    }
}
