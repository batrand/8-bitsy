﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
[RequireComponent(typeof(BoxCollider2D))]
public class WaterGush : MonoBehaviour
{
    private SpriteRenderer _renderer;
    private BoxCollider2D _collider;
    private void Start()
    {
        _renderer = GetComponent<SpriteRenderer>();
        _collider = GetComponent<BoxCollider2D>();

        if(IntervalSeconds > 0) StartInterval();
    }

    public Vector2 OpenSize = new Vector2(1f,1f);
    public Vector2 ClosedSize = new Vector2(0f,0f);
    public float SizeChangeSpeed = 5f;

    public float IntervalSeconds = 3f;

    public void StartOpening()
    {
        StopAllCoroutines();
        StartCoroutine(nameof(StartOpeningCoroutine));
    }
    private IEnumerator StartOpeningCoroutine()
    {
        while (RendererIsNotOpen && ColliderIsNotOpen)
        {
            _renderer.size = Vector2.Lerp(_renderer.size, OpenSize, Time.deltaTime * SizeChangeSpeed);
            _collider.size = Vector2.Lerp(_collider.size, OpenSize, Time.deltaTime * SizeChangeSpeed);
            yield return 0;
        }
    }

    public void StartClosing()
    {
        StopAllCoroutines();
        StartCoroutine(nameof(StartClosingCoroutine));
    }

    private IEnumerator StartClosingCoroutine()
    {
        while (RendererIsNotClosed && ColliderIsNotClosed)
        {
            _renderer.size = Vector2.Lerp(_renderer.size, ClosedSize, Time.deltaTime * SizeChangeSpeed);
            _collider.size = Vector2.Lerp(_collider.size, ClosedSize, Time.deltaTime * SizeChangeSpeed);
            yield return 0;
        }
    } 

    private bool RendererIsNotOpen => _renderer.size.x < OpenSize.x || _renderer.size.y < OpenSize.y;
    private bool ColliderIsNotOpen => _collider.size.x < OpenSize.x || _collider.size.y < OpenSize.y;
    private bool RendererIsNotClosed => _renderer.size.x > ClosedSize.x || _renderer.size.y > ClosedSize.y;
    private bool ColliderIsNotClosed => _collider.size.x > ClosedSize.x || _collider.size.y > ClosedSize.y;

    private void StartInterval()
    {
        StartOpening();
        Invoke(nameof(StopInterval), IntervalSeconds);
    }

    private void StopInterval()
    {
        StartClosing();
        Invoke(nameof(StartInterval), IntervalSeconds);
    }
}
