﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class Deadly : MonoBehaviour
{
    /// <summary>
    /// If an object with this tag touches this, trigger game over.
    /// </summary>
    public string TargetTag = "BITSY";

    public float DeathDelay = 2f;

    private Game _game;
    private void Start()
    {
        _game = FindObjectOfType<Game>();
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.HasTag(TargetTag))
        {
            var bitsyDeath = other.gameObject.GetComponent<BitsyDeath>();
            if (bitsyDeath != null) bitsyDeath.Die();

            Invoke(nameof(CallGameOver), DeathDelay);
        }
    }

    private void CallGameOver() => _game.GameIsOver();
}
