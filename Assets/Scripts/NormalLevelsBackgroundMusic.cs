﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NormalLevelsBackgroundMusic : MonoBehaviour
{
    private BackgroundMusic _music;
    private void Start()
    {
        _music = BackgroundMusic.Instance;
        _music.NormalLevels();
    }
}
