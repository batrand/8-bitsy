﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Edge = PlayerEdgeStates.Edge;
using EdgeStateChangeEventArgs = PlayerEdgeStates.EdgeStateChangeEventArgs;
using EdgeState = PlayerEdgeStates.EdgeState;

[RequireComponent(typeof(PlayerEdgeStates))]
public class PlayerEdgeStatesDebugView : MonoBehaviour
{
    public string TopState;
    public string BottomState;
    public string LeftState;
    public string RightState;

    [Space]

    public bool TopCanJump;
    public bool BottomCanJump;
    public bool LeftCanJump;
    public bool RightCanJump;

    [Space]

    public bool TopShouldAutoJump;
    public bool BottomShouldAutoJump;
    public bool LeftShouldAutoJump;
    public bool RightShouldAutoJump;

    [Space]

    public bool TopIsStuck;
    public bool BottomIsStuck;
    public bool LeftIsStuck;
    public bool RightIsStuck;

    [Space]

    public string TopTouchables;
    public string BottomTouchables;
    public string LeftTouchables;
    public string RightTouchables;

    private PlayerEdgeStates _states;
	private void Start ()
	{
	    _states = GetComponent<PlayerEdgeStates>();
	}
	
	private void Update ()
	{
	    TopState = _states.EdgeStates[Edge.Top].ToString();
	    BottomState = _states.EdgeStates[Edge.Bottom].ToString();
	    LeftState = _states.EdgeStates[Edge.Left].ToString();
	    RightState = _states.EdgeStates[Edge.Right].ToString();

	    TopIsStuck = _states.IsStuck(Edge.Top);
	    BottomIsStuck = _states.IsStuck(Edge.Bottom);
	    LeftIsStuck = _states.IsStuck(Edge.Left);
	    RightIsStuck = _states.IsStuck(Edge.Right);

	    TopCanJump = _states.CanJump(Edge.Top);
	    BottomCanJump = _states.CanJump(Edge.Bottom);
	    LeftCanJump = _states.CanJump(Edge.Left);
	    RightCanJump = _states.CanJump(Edge.Right);

	    TopShouldAutoJump = _states.ShouldAutoJump(Edge.Top);
	    BottomShouldAutoJump = _states.ShouldAutoJump(Edge.Bottom);
	    LeftShouldAutoJump = _states.ShouldAutoJump(Edge.Left);
	    RightShouldAutoJump = _states.ShouldAutoJump(Edge.Right);

        TopTouchables = $"[{_states.EdgeTouchables[Edge.Top].Count}] ";
	    foreach (var touchable in _states.EdgeTouchables[Edge.Top])
	    {
	        var gobj = touchable.gameObject;
	        var type = touchable.GetType().ToString();
            TopTouchables += $"{gobj.name} ({type}), ";
	    }

	    BottomTouchables = $"[{_states.EdgeTouchables[Edge.Bottom].Count}] ";
	    foreach (var touchable in _states.EdgeTouchables[Edge.Bottom])
	    {
	        var gobj = touchable.gameObject;
	        var type = touchable.GetType().ToString();
            BottomTouchables += $"{gobj.name} ({type}), ";
	    }

	    LeftTouchables = $"[{_states.EdgeTouchables[Edge.Left].Count}] ";
	    foreach (var touchable in _states.EdgeTouchables[Edge.Left])
	    {
	        var gobj = touchable.gameObject;
	        var type = touchable.GetType().ToString();
            LeftTouchables += $"{gobj.name} ({type}), ";
	    }

	    RightTouchables = $"[{_states.EdgeTouchables[Edge.Right].Count}] ";
	    foreach (var touchable in _states.EdgeTouchables[Edge.Right])
	    {
	        var gobj = touchable.gameObject;
	        var type = touchable.GetType().ToString();
            RightTouchables += $"{gobj.name} ({type}), ";
	    }
    }
}
