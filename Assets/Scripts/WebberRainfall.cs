﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WebberRainfall : MonoBehaviour
{

    public GameObject webbable;
    public float spawnTime;
    public float Speed;
    public float DestroyY;
    public float rotationZ;
    public string LAYER_NAME;
    public bool isSticky;
    public bool lockBitsyPosition = true;

    private List<GameObject> _spawns = new List<GameObject>();

    // Use this for initialization
    void Start()
    {
        SpawnWebber();
    }

    void SpawnWebber()
    {
        var spawn = GameObject.Instantiate(
            webbable, new Vector3(transform.position.x, transform.position.y, 0), Quaternion.Euler(0, 0, rotationZ));
        spawn.GetComponent<SpriteRenderer>().sortingLayerName = LAYER_NAME;
        spawn.GetComponent<Webbable>().IsSticky = this.isSticky;
        if (lockBitsyPosition) spawn.AddComponent<StickyMover>();
        _spawns.Add(spawn);
        Invoke("SpawnWebber", spawnTime);
    }

    void Update()
    {
        foreach (var spawn in _spawns)
        {
            spawn.transform.position += Vector3.down * Speed * Time.deltaTime;
            if (spawn.transform.position.y < DestroyY)
            {
                _spawns.Remove(spawn);
                GameObject.Destroy(spawn);
                break;
            }
        }
    }
}