﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LastLevelBackgroundMusic : MonoBehaviour
{
    private BackgroundMusic _music;
    private void Start()
    {
        _music = BackgroundMusic.Instance;
        _music.Tense();
    }

    public void Chase() => _music.Chase();
    public void Reunion() => _music.Reunion();
}
