﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Maintains the states of the edges of a collider.
/// </summary>
[RequireComponent(typeof(Collider2D))]
public class PlayerEdgeStates : MonoBehaviour
{
    [Range(1, 4)]
    public int Resolution = 3;

    private BoxCollider2D _collider;
    private void Start()
    {
        _collider = GetComponent<BoxCollider2D>();
    }

    [Tooltip("Draw in Edit mode the raycasts")]
    public bool DrawDebug = true;
    public Color HitColor = Color.green;
    public float RaycastLength = 0.3f;
    [Tooltip("Add this tag to a Tags component and that object " +
             "will not be considered as Touching this object")]
    public string IgnoreTag = "IGNORE";

    public enum Edge { Top, Bottom, Left, Right }
    public enum EdgeState
    {
        InAir, // Not touching anything
        Touching, // Touching something
    }
    public Dictionary<Edge, EdgeState> EdgeStates = new Dictionary<Edge, EdgeState>
    {
        { Edge.Top, EdgeState.InAir },
        { Edge.Bottom, EdgeState.InAir },
        { Edge.Left, EdgeState.InAir },
        { Edge.Right, EdgeState.InAir }
    };

    public bool IsInAir(Edge edge) => EdgeStates[edge] == EdgeState.InAir;
    /// <summary>
    /// The touchables each edge is having
    /// </summary>
    public Dictionary<Edge, List<MonoBehaviour>> EdgeTouchables = new Dictionary<Edge, List<MonoBehaviour>>
    {
        { Edge.Top, new List<MonoBehaviour>() },
        { Edge.Bottom, new List<MonoBehaviour>() },
        { Edge.Left, new List<MonoBehaviour>() },
        { Edge.Right, new List<MonoBehaviour>() },
    };

    public bool HasTouchable(Edge edge, MonoBehaviour touchable)
    {
        if (touchable == null) return false;
        foreach (var edgeTouchable in EdgeTouchables[edge])
        {
            if (edgeTouchable == null) continue;
            if (edgeTouchable.GetInstanceID() == touchable.GetInstanceID())
                return true;
        }
        return false;
    }

    public bool HasTouchable(MonoBehaviour touchable)
    {
        return HasTouchable(Edge.Top, touchable)
               || HasTouchable(Edge.Bottom, touchable)
               || HasTouchable(Edge.Left, touchable)
               || HasTouchable(Edge.Right, touchable);
    }

    /// <summary>
    /// Returns true when can jump from this edge
    /// </summary>
    public bool CanJump(Edge edge)
    {
        if (IsInAir(edge)) return false;

        // Is touching something
        var touchables = EdgeTouchables[edge];
        foreach (var touchable in touchables)
        {
            if (touchable is Jumpable) return true;
        }

        return false;
    }

    public bool ShouldAutoJump(Edge edge)
    {
        var canJump = CanJump(edge);
        if (!canJump) return false;

        var touchables = EdgeTouchables[edge];
        foreach (var touchable in touchables)
        {
            if (touchable is Jumpable)
            {
                var jumpable = (Jumpable)touchable;
                if (jumpable.IsAuto) return true;
            }
        }

        return false;
    }

    public bool IsStuck(Edge edge)
    {
        if (IsInAir(edge)) return false;

        // There is unsticking to do
        if (_unstickStartTime != -1f)
        {
            // Unstick grace has not expired
            if (_unstickStartTime + _unstickGrace >= Time.time)
                return false;
            // Unstick grace expired
            else
            {
                _unstickStartTime = -1f;
                _unstickGrace = -1f;
            }
        }

        // Is touching something
        var touchables = EdgeTouchables[edge];
        foreach (var touchable in touchables)
        {
            if (touchable is Webbable)
            {
                var webbable = (Webbable)touchable;
                if (webbable.ShouldStick) return true;
            }
        }
        return false;
    }

    public bool IsSlippery(Edge edge)
    {
        if (IsInAir(edge)) return false;

        foreach (var touchable in EdgeTouchables[edge])
            if (touchable is Slippery)
                return true;
        return false;
    }

    public float GetSlipSeconds(Edge edge)
    {
        if (!IsSlippery(edge)) return 0;
        float slipSeconds = 0;
        foreach (var touchable in EdgeTouchables[edge])
            if (touchable is Slippery)
                slipSeconds += (touchable as Slippery).SlipSeconds;
        return slipSeconds;
    }

    public bool IsSlipperySlope(Edge edge)
    {
        if (!IsSlippery(edge)) return false;
        foreach (var touchable in EdgeTouchables[edge])
            if (touchable is Slippery)
            {
                bool isSlope = (touchable as Slippery).IsSlope;
                if (isSlope) return true;
            }
        return false;
    }

    public float GetSlopeMultiplier(Edge edge)
    {
        if (!IsSlippery(edge)) return 0;
        float multiplier = 0;
        foreach (var touchable in EdgeTouchables[edge])
            if (touchable is Slippery)
                multiplier += (touchable as Slippery).SlopeGravityMultiplier;
        return multiplier;
    }

    /// <summary>
    /// Apply unstick grace to allow unsticking without immediately
    /// getting stuck again
    /// </summary>
    /// <param name="grace">Number of seconds to disable stickiness</param>
    public void Unstick(float grace)
    {
        _unstickStartTime = Time.time;
        _unstickGrace = grace;
    }
    // Game time when the unsticking started. -1 to disable
    private float _unstickStartTime = -1f;
    // Duration of the unsticking grace, after which stickiness is resumed. -1 to disable
    private float _unstickGrace = -1f;

    public class EdgeStateChangeEventArgs : EventArgs
    {
        public Edge Edge;
        public EdgeState OldState;
        public EdgeState NewState;
    }
    public event EventHandler<EdgeStateChangeEventArgs> OnEdgeStateChangedEvent;

    private void Update()
    {
        CheckForTopHits();
        CheckForBottomHits();
        CheckForLeftHits();
        CheckForRightHits();
    }

    #region Raycasts
    private void CheckForBottomHits()
    {
        var startCenter = transform.position;
        var halfWidth = _collider.bounds.extents.x;

        var startLeft = startCenter;
        startLeft.x -= halfWidth;
        var startRight = startCenter;
        startRight.x += halfWidth;

        var starts = new List<Vector3> { startLeft, startCenter, startRight };

        var endCenter = startCenter;
        endCenter.y -= RaycastLength;
        var endLeft = endCenter;
        endLeft.x = startLeft.x;
        var endRight = endCenter;
        endRight.x = startRight.x;

        var ends = new List<Vector3> { endLeft, endCenter, endRight };

        DoRaycasts(Edge.Bottom, starts, ends);
    }

    private void CheckForTopHits()
    {
        var startCenter = transform.position;
        var halfWidth = _collider.bounds.extents.x;

        var startLeft = startCenter;
        startLeft.x -= halfWidth;
        var startRight = startCenter;
        startRight.x += halfWidth;

        var starts = new List<Vector3> { startLeft, startCenter, startRight };

        var endCenter = startCenter;
        endCenter.y += RaycastLength;
        var endLeft = endCenter;
        endLeft.x = startLeft.x;
        var endRight = endCenter;
        endRight.x = startRight.x;

        var ends = new List<Vector3> { endLeft, endCenter, endRight };

        DoRaycasts(Edge.Top, starts, ends);
    }

    private void CheckForLeftHits()
    {
        var startCenter = transform.position;
        var halfHeight = _collider.bounds.extents.y;

        var startTop = startCenter;
        startTop.y += halfHeight;
        var startBottom = startCenter;
        startBottom.y -= halfHeight;

        var starts = new List<Vector3> { startTop, startCenter, startBottom };

        var endCenter = startCenter;
        endCenter.x -= RaycastLength;
        var endTop = endCenter;
        endTop.y = startTop.y;
        var endBottom = endCenter;
        endBottom.y = startBottom.y;

        var ends = new List<Vector3> { endTop, endCenter, endBottom };

        DoRaycasts(Edge.Left, starts, ends);
    }

    private void CheckForRightHits()
    {
        var startCenter = transform.position;
        var halfHeight = _collider.bounds.extents.y;

        var startTop = startCenter;
        startTop.y += halfHeight;
        var startBottom = startCenter;
        startBottom.y -= halfHeight;

        var starts = new List<Vector3> { startTop, startCenter, startBottom };

        var endCenter = startCenter;
        endCenter.x += RaycastLength;
        var endTop = endCenter;
        endTop.y = startTop.y;
        var endBottom = endCenter;
        endBottom.y = startBottom.y;

        var ends = new List<Vector3> { endTop, endCenter, endBottom };

        DoRaycasts(Edge.Right, starts, ends);
    }

    private void DoRaycasts(Edge edge, List<Vector3> starts, List<Vector3> ends)
    {
        // Add additional resolutions
        starts = ApplyResolution(starts);
        ends = ApplyResolution(ends);

        EdgeTouchables[edge].Clear();

        bool hasAtLeastOneHit = false;
        for (var i = 0; i < starts.Count; i++)
        {
            if (DrawDebug) Debug.DrawLine(starts[i], ends[i], Color.white);
            if (ProcessHit(transform.FindUnblockedHit(starts[i], ends[i]), edge))
            {
                hasAtLeastOneHit = true;
                if (DrawDebug) Debug.DrawLine(starts[i], ends[i], HitColor);
            }
        }

        var oldState = EdgeStates[edge];
        if (hasAtLeastOneHit) EdgeStates[edge] = EdgeState.Touching;
        else EdgeStates[edge] = EdgeState.InAir;
        CheckForStateChange(edge, oldState, EdgeStates[edge]);
    }

    private List<Vector3> ApplyResolution(List<Vector3> original)
    {
        var starts = original;
        for (int res = 2; res <= Resolution; res++)
        {
            var newStarts = new List<Vector3>();
            for (int i = 0; i < starts.Count; i++)
            {
                newStarts.Add(starts[i]);
                if (i == starts.Count - 1) break;

                var previous = starts[i];
                var next = starts[i + 1];
                var avg = (previous + next) / 2;
                newStarts.Add(avg);
            }
            starts = newStarts;
        }
        return starts;
    }
    #endregion

    private bool ProcessHit(RaycastHit2D? hit, Edge edge)
    {
        if (hit == null) return false;

        var hitObj = hit.Value.transform.gameObject;

        if (hitObj.HasTag(IgnoreTag)) return false;

        if (hitObj.HasComponent<Jumpable>())
            AddTouchable(edge, hitObj.GetComponent<Jumpable>());
        if (hitObj.HasComponent<Webbable>())
            AddTouchable(edge, hitObj.GetComponent<Webbable>());
        if (hitObj.HasComponent<Slippery>())
            AddTouchable(edge, hitObj.GetComponent<Slippery>());
        if (hitObj.HasComponent<StickyMover>())
            AddTouchable(edge, hitObj.GetComponent<StickyMover>());

        return true;
    }

    private void AddTouchable(Edge edge, MonoBehaviour touchable)
    {
        if (touchable == null) return;
        if (!(touchable is Jumpable) 
            && !(touchable is Webbable)
            && !(touchable is Slippery)
            && !(touchable is StickyMover)) return;

        if (!HasTouchable(edge, touchable))
            EdgeTouchables[edge].Add(touchable);
    }

    private void CheckForStateChange(Edge edge, EdgeState oldState, EdgeState newState)
    {
        if (oldState == newState) return;
        OnEdgeStateChangedEvent?.Invoke(this, new EdgeStateChangeEventArgs()
        {
            Edge = edge,
            OldState = oldState,
            NewState = newState
        });
    }
}
