﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

public class StickyMover : MonoBehaviour
{
    private List<GameObject> _stickees = new List<GameObject>();

    private Vector3 _previousLocation;
    private void Start()
    {
        _previousLocation = transform.position;
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        _stickees.Add(other.gameObject);
    }
    private void OnCollisionExit2D(Collision2D other)
    {
        _stickees.Remove(other.gameObject);
    }

    private void Update()
    {
        var newLocation = transform.position;

        var xDelta = newLocation.x - _previousLocation.x;
        var yDelta = newLocation.y - _previousLocation.y;

        foreach (var stickee in _stickees)
        {
            var webber = stickee.GetComponent<Webber>();
            if (webber != null)
            {
                if (webber.IsMovingToTarget)
                {
                    _stickees.Remove(stickee);
                    return;
                }
            }

            var stickeePos = stickee.transform.position;
            stickeePos.x += xDelta;
            stickeePos.y += yDelta;
            stickee.transform.position = stickeePos;
        }

        _previousLocation = newLocation;
    }
}
