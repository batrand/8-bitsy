﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Collider2D))]
public class TouchTrigger : MonoBehaviour
{
    [Tooltip("What will happen when this trigger is activated")]
    public UnityEvent OnActivateEvent;
    
    [Tooltip("This trigger can only be used once")]
    public bool OneUseOnly = false;
    private bool _hasBeenUsed = false;

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (OneUseOnly && _hasBeenUsed) return;
        else
        {
            OnActivateEvent.Invoke();
            _hasBeenUsed = true;
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (OneUseOnly && _hasBeenUsed) return;
        else
        {
            OnActivateEvent.Invoke();
            _hasBeenUsed = true;
        }
    }
}
