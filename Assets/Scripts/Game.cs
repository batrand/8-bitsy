﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Game : MonoBehaviour
{
    public GameObject GameOverPanel;
    public GameObject GamePausedPanel;

    public string PauseButton = "escape";

    private void Start()
    {
        GameOverPanel.SetActive(false);
        GamePausedPanel.SetActive(false);
        Resume();
    }

    private void Update()
    {
        if (Input.GetKeyDown(PauseButton))
            Pause();
    }

    private bool _isGameOver = false;
    public void GameIsOver()
    {
        _isGameOver = true;
        GameOverPanel.SetActive(true);
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void Pause()
    {
        if (_isGameOver) return;
        GamePausedPanel.SetActive(true);
        Time.timeScale = 0;
    }

    public void Resume()
    {
        GamePausedPanel.SetActive(false);
        Time.timeScale = 1;
    }
}
