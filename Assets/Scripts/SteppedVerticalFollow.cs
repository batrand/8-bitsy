﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Follows target along the Y axis upward in pairs of steps
/// </summary>
public class SteppedVerticalFollow : MonoBehaviour
{
    public GameObject Target;
    public float FollowSpeed = 15f;
    public bool Smoothed = true;
    [Tooltip("The points once past, will move to center of next pair")]
    public float[] Steps;

    private void Update()
    {
        if (Target == null) return;
        if (Steps == null || Steps.Length < 2) return;

        var finalPosition = new Vector3(transform.position.x, GetCameraY(), transform.position.z);

        Vector3 intermediatePosition;
        if (Smoothed)
            intermediatePosition = Vector3.Lerp(
                transform.position, finalPosition,
                FollowSpeed * Time.deltaTime);
        else
            intermediatePosition = Vector3.MoveTowards(
                transform.position, finalPosition,
                FollowSpeed * Time.deltaTime);
        transform.position = intermediatePosition;
    }

    private float GetCameraY()
    {
        var targetY = Target.transform.position.y;

        // Find the range the Target is in
        float rangeBottom = 0, rangeTop = 0;
        bool foundValidRange = false;
        for (var i = 0; i < Steps.Length; i++)
        {
            if (i + 1 >= Steps.Length) break;
            if (targetY >= Steps[i] && targetY <= Steps[i + 1])
            {
                rangeBottom = Steps[i];
                rangeTop = Steps[i + 1];
                foundValidRange = true;
                break;
            }
        }

        if (!foundValidRange)
        {
            Debug.LogWarning("Could not find valid range; defaulting to first pair");
            rangeBottom = Steps[0];
            rangeTop = Steps[1];
        }

        return (rangeBottom+rangeTop)/2;
    }
}
