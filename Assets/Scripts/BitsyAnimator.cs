﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BitsyAnimator : MonoBehaviour {

    Animator anim;

	// Use this for initialization
    private float leftX, rightX;
    private float aliveY, deadY;
	void Start () {
        anim = GetComponent<Animator>();
	    rightX = transform.localScale.x;
	    leftX = -rightX;
	    aliveY = transform.localScale.y;
	    deadY = -aliveY;
	}
	
	// Update is called once per frame
	void Update () {

	    var currentScale = transform.localScale;

        //Walking to the right
        if (Input.GetKeyDown(KeyCode.D)) {
            anim.SetInteger("State", 1);
            currentScale.x = rightX;
        }
        if (Input.GetKeyUp(KeyCode.D)) {
            anim.SetInteger("State", 0);
        }

        //Wealking to the left
        if (Input.GetKeyDown(KeyCode.A)) {
            anim.SetInteger("State", 1);
            currentScale.x = leftX;
        }
        if (Input.GetKeyUp(KeyCode.A)) {
            anim.SetInteger("State", 0);
        }

        //Jumping
	    if (Input.GetKeyDown(KeyCode.Space)) {
	        anim.SetInteger("State", 2);
	    }
	    if (Input.GetKeyUp(KeyCode.Space))
	    {
	        anim.SetInteger("State", 0);
	    }

        transform.localScale = currentScale;
    }
}