﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class Jumpable : MonoBehaviour
{
    [Tooltip("If true, touching this Jumpable will automatically " +
             "applies the jump")]
    public bool IsAuto = false;
}
